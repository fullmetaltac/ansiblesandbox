const express = require('express');
const pg = require('pg');
const fs = require("fs");

const app = express();

const connectionString = 'postgresql://postgres:postgres@localhost:5432/myapp'

const client = new pg.Client(connectionString);
client.connect();

app.get("/", (req, res) => {
    client.query('SELECT * FROM logs;', (qerr, qres) => {
        if (qres) {
            fs.readFile(qres.rows[0].location, "utf8", (err, data) => {
                res.send(data);
            });
        }
        else {
            res.send('No logs found!');
        }
        client.end()
    })
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});